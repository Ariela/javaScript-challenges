
function fileReadAndParse() {
    //get file to input
    var fs = require("fs");
    var file = fs.readFileSync("Data.txt");
    var lines = file.toString().trim().split('\n');
    var output = [];

    //loop thru file and split up to get values, set in array
    for (var i in lines) {
        var valueList = lines[i].substr(29).replace(/None/g, '').replace(/,/g, ' ').trim();
        //creates a new array with the results of each new element that is determened by the function
        var valueArray = valueList.split(' ').map(function (v) { return parseFloat(v)});
        console.log(valueArray);
        //put each item into a single array
        output.push({
            hostName: lines[i].substr(0, 3),
            //the total is the first eleemnt in the array, value is the second
            //loops like a for loop and add each value to the total until its empty
            avg : valueArray.reduce((total, value) => total + value, 0) / valueArray.length,
            max : Math.max.apply(null, valueArray),
            min : Math.min.apply(null, valueArray)
        });
    }
    //sor array by average value
    var sortedList = output.sort(function(previous,current){
        //use - to compare numbers instead of strings
        return current.avg - previous.avg;
    });
 //loop through new sorted list and display
    for(var s in sortedList){
  //      console.log(sortedList[s].hostName + ":" + " Average:" + sortedList[s].avg.toFixed(1) + " Max:" + sortedList[s].max.toFixed(1) + " Min:" + sortedList[s].min.toFixed(1));
    }
}

fileReadAndParse();
