/*back ticks*/
let b = 'world';
let d = `hello ${b}`;
console.log(d);
//======================================
/*spread operator*/
let a = [7,8,9];
let b = [6,...a, 10]
console.log(b);
console.log(...a);
//======================================
// function butter() {
//     let a = [1, 2, 3];
//     let b = [4,5,6]
//     let c = [...a,...b];
//     return c;
// }
//
// butter(4, 5, 6);

/*Destructuring Assignments with arrays and objects */
let c = [100, 200];
let [a,b] = c;
console.log(a,b);

let wizard = {
   magical:true,
   power: 10
}
let {magical, power } = wizard;
console.log(magical, power);

const lion = {
   size: 'large',
   sound: 'roar'
}

let{sound} = lion;
console.log(sound);

/* Arrow functions - anonmymous funcitons
benefits -
1. arrow funcitons do not bind thier own this */
setTimeout(function()
{
  console.log("sound");
}, 1000);

// setTimeout(() =>{
//     console.log("sound");
// }, 1000);
//
// let eye = "eye";
//
// const fire = () =>
// {
//     return `bulls-${eye}`;
// };
//
// let lengths = [3, 7, 5];
// let multiple = 8;
// const scale = lengths.map(x => x * multiple);

/* Filtering*/
let scores  = [90,85,67, 71,70, 55,92];
let passing = scores.filter(elements => elements >= 70);
console.log(passing);
